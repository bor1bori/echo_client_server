#include <stdio.h> // for perror
#include <string.h> // for memset
#include <unistd.h> // for close
#include <arpa/inet.h> // for htons
#include <netinet/in.h> // for sockaddr_in
#include <sys/socket.h> // for socket
#include <thread>
#include <iostream>

using namespace std;

void usage() {
	printf("usage: echo_client <host_ip> <port> \nexample: echo_client 127.0.0.1 1234\n");
}

int parse_ip(char *ip_str) {
  int i = 0;
  int result = 0;
  char *ptr = strtok(ip_str, ".");
  while (ptr != NULL) {
	result = result << 8;
    if (i >= 4) {
      return -1;
    }
	result += atoi(ptr);
    ptr = strtok(NULL, ".");
    i++;
  }
  if (i != 4) {
    return -1;
  }
  return result;
}

int recieve_runner(int sockfd) {
	while (true) {
		const static int BUFSIZE = 1024;
		char buf[BUFSIZE];
		ssize_t received = recv(sockfd, buf, BUFSIZE - 1, 0);
		if (received == 0 || received == -1) {
			perror("recv failed");
			break;
		}
		buf[received] = '\0';
		printf("%s\n", buf);
	}
}
int main(int argc, char* argv[]) {
	int ip, port;

	if (argc != 3) {
		usage();
		return -1;
	}

	ip = parse_ip(argv[1]);
	port = atoi(argv[2]);
	
	if (ip == -1 || port == 0) {
		usage();
		return -1;
	}

	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		perror("socket failed");
		return -1;
	}

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(1234);
	addr.sin_addr.s_addr = htonl(ip);
	memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

	int res = connect(sockfd, reinterpret_cast<struct sockaddr*>(&addr), sizeof(struct sockaddr));
	if (res == -1) {
		perror("connect failed");
		return -1;
	}
	printf("connected\n");

	while (true) {
		const static int BUFSIZE = 1024;
		char buf[BUFSIZE];

		scanf("%s", buf);
		if (strcmp(buf, "quit") == 0) break;

		ssize_t sent = send(sockfd, buf, strlen(buf), 0);
		if (sent == 0) {
			perror("send failed");
			break;
		}
		thread t(&recieve_runner, sockfd);
		t.detach();
	}
	close(sockfd);
}
