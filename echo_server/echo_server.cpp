#include <stdio.h> // for perror
#include <string.h> // for memset
#include <unistd.h> // for close
#include <arpa/inet.h> // for htons
#include <netinet/in.h> // for sockaddr_in
#include <sys/socket.h> // for socket
#include <thread>
#include <iostream>
#include <list>
#include <mutex>

using namespace std;

mutex mtx;

void usage() {
	printf("usage: echo_server <port> [-b] (-b : echo to broadcast)\nexample: echo_server 1234 -b\n");
}

int send_broadcast(list<int> *fds, char * buf) {
	for (list<int>::iterator it = fds->begin(); it != fds->end() ; it ++) {
		ssize_t sent = send((*it), buf, strlen(buf), 0);
		if (sent == 0) {
			mtx.lock();
			fds->remove((*it));
			mtx.unlock();
			perror("send failed");
		}
	}
}

int echo_runner(int childfd, list<int> *fds, int b_flag) {
	mtx.lock();
	fds->push_back(childfd);
	mtx.unlock();

	while (true) {
		const static int BUFSIZE = 1024;
		char buf[BUFSIZE];

		ssize_t received = recv(childfd, buf, BUFSIZE - 1, 0);
		if (received == 0 || received == -1) {
			perror("recv failed");
			break;
		}
		buf[received] = '\0';
		printf("%s\n", buf);
		if (b_flag) {
			send_broadcast(fds, buf);
		} else {
			ssize_t sent = send(childfd, buf, strlen(buf), 0);
			if (sent == 0) {
				perror("send failed");
				break;
			}
		}
	}

	mtx.lock();
	fds->remove(childfd);
	mtx.unlock();
}

int main(int argc, char* argv[]) {
	list<int> fds;
	mutex mutex_lock;
	int port = 0, b_flag = 0;
	if (!(argc == 2 || argc == 3)) {
		usage();
		return -1;
	}
	port = atoi(argv[1]);
	if (port == 0) {
		usage();
		return -1;
	}
	if (argc == 3) {
		if (strncmp(argv[2], "-b", 2)) {
			usage();
			return -1;			
		} else {
			b_flag = 1;
		}
	}

	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		perror("socket failed");
		return -1;
	}

	int optval = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,  &optval , sizeof(int));

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

	int res = bind(sockfd, reinterpret_cast<struct sockaddr*>(&addr), sizeof(struct sockaddr));
	if (res == -1) {
		perror("bind failed");
		return -1;
	}

	res = listen(sockfd, 2);
	if (res == -1) {
		perror("listen failed");
		return -1;
	}

	while (true) {
		struct sockaddr_in addr;
		socklen_t clientlen = sizeof(sockaddr);
		int childfd = accept(sockfd, reinterpret_cast<struct sockaddr*>(&addr), &clientlen);
		if (childfd < 0) {
			perror("ERROR on accept");
			break;
		}
		printf("connected\n");
		thread t(&echo_runner, childfd, &fds, b_flag);
		t.detach();
	}
	close(sockfd);
}
